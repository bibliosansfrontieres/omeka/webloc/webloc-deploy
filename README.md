# Deploy WebLOC

## Requirements

### Sharepoint application

You need to create a Sharepoint application for WebLOC backend.

There is a documenation on [How to create a Sharepoint application](sharepoint.md)

### Azure Active Directory application

You need to create an Azure Active Directory application for WebLOC frontend.

There is a documentation on [How to create an Azure Active Directory application](azure-active-directory.md)

## Prepare

Clone this git repository :

`git clone git@gitlab.com:bibliosansfrontieres/omeka/webloc/webloc-deploy.git`

Go into the new directory and clone webloc repositories :

```
cd webloc-deploy
git clone git@gitlab.com:bibliosansfrontieres/omeka/webloc/webloc-backend.git
git clone git@gitlab.com:bibliosansfrontieres/omeka/webloc/webloc-frontend.git
```

## Env files

### Backend env file

For developement and production, you need a file named `.env` in the folder.

```
cp webloc-backend/.env.example webloc-backend/.env
nano .env
```

Just fill values that you have in .env file (all needed).

### Frontend env file

For developement, you need a file named `.env.local` and `.env.production` for production.

Developement :

```
cp webloc-frontend/.env.example webloc-frontend/.env.local
nano webloc-frontend/.env.local
```

Production :

```
cp webloc-frontend/.env.example webloc-frontend/.env.production
nano webloc-frontend/.env.production
```

Just fill values that you have in .env file (all needed).

## Run

### Run in production

Don't forget to use `.env.production` and not `.env.local` for webloc-frontend !

To run the project, just use :

`docker compose up -d`

### Run in developement

There are 2 ways to run in developement environment :

- Run with Docker
- Run without Docker

To run it with docker, you need to use `.env.production` for webloc-frontend, like with production environment.

#### Run without docker in developement

Run backend :

```
cd webloc-backend
npm run install
npm run start:dev
```

Run frontend :

Don't forget to use `.env.local` for webloc-frontend !

```
cd webloc-frontend
npm run install
npm run dev
```