# Create a Sharepoint application

## Register a new app

Go at `https://ORGANIZATION.sharepoint.com/sites/SITENAME/_layouts/15/appregnew.aspx`

_(You need to replace `ORGANIZATION` and `SITENAME` with your informations)_

- Generate a client ID
- Generate a client secret
- Choose the title you want
- Choose an application domain (not really important)
- Choose your redirect URL (not really important)
- Click on `Create` button
- Copy informations given (Client id, client secret)

## Give rights to your Sharepoint application

Go at `https://ORGANIZATION.sharepoint.com/sites/SITENAME/_layouts/15/appinv.aspx`

_(You need to replace `ORGANIZATION` and `SITENAME` with your informations)_

- Put your client ID and click on `search`
- Put this XML code :

```
<AppPermissionRequests AllowAppOnlyPolicy="true">
  <AppPermissionRequest Scope="http://sharepoint/content/tenant" Right="FullControl" />
</AppPermissionRequests>
```

- Click on `Create` button

## Get tenant ID

Go at `https://ORGANIZATION.sharepoint.com/sites/SITENAME/_layouts/15/appprincipals.aspx`

You will see your app with an app identificator.

Example : `000000000-0000-0000-0000-00000000@xxxxx-xxx-xxx-xxx-xxxxx`

The ID that you see after `@` is your tenant ID.

## Get Sharepoint base folder

Go at `https://ORGANIZATION.sharepoint.com/sites/SITENAME/`

- Click on `Documents`

You will get an URL like `https://ORGANIZATION.sharepoint.com/sites/SITENAME/Documents partages/Forms/AllItems.aspx`

`Documents partages` is the base folder here

## Get Sharepoint site name

Go at `https://ORGANIZATION.sharepoint.com/`

- Search your site in the search bar and click on it

You will get an URL like `https://ORGANIZATION.sharepoint.com/sites/SITENAME`

`SITENAME` is your site name here.