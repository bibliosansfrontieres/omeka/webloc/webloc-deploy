# Create an Azure Active Directory application

Go to <https://entra.microsoft.com/>

- Clic on `Applications` -> `Registered Apps`
- Clic on `New registration`
- Give it a name as you wish
- Select what account types are accepted
- Select `Web` for URL redirection
- Give this URL redirection :

Production : `https://domain.com/api/auth/callback/azure-ad`

Development : `http://localhost:3000/api/auth/callback/azure-ad`

- Clic on `Register`
- Copy app ID (client) and tenant ID (just under object ID)
- Clic on `Certificates & secrets`
- Clic on `New client secret`
- Give the description as you wish and an expiration date
- Clic on `Add`
- Copy the value (not ID of secret)

You have now an application ID and secret, with the tenant ID !


